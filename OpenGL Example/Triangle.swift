
import GLKit

class Triangle {
    
    // --- your model ---
    let vertices:[GLfloat] =
        [1,-0.5,0,
         0,0.5,0,
         -1,-0.5,0]

    let indices:[GLushort] = [0,1,2] //the order in which the vertices are drawn, important for determining the front of each triangle
    
    let texCoords:[GLfloat] =
        [0,1,
         0.5,0,
         1,1]
    
    let normals:[GLfloat] =
        [1,0,0,
         0,1,0,
         -1,0,0]
    // ------------------
    
    var vertexArray:GLuint = 0
    var vertexBuffer:GLuint = 0
    var indexBuffer:GLuint = 0
    var textureBuffer:GLuint = 0
    var normalBuffer:GLuint = 0
    
    var effect:Effect!
    
    var mvm:GLKMatrix4! //model view matrix
    
    var position:GLKVector3 = GLKVector3Make(0, 0, 0)
    var front:GLKVector3 = GLKVector3Make(0, 0, 1)
    var up:GLKVector3 = GLKVector3Make(0, 1, 0)
    var right:GLKVector3 = GLKVector3Make(1, 0, 0)
    var rotationDirection = 1
    
    func setup (effect e:Effect) {
        
        effect = e
        
        //Create Vertex Array Object (VAO)
        glGenVertexArraysOES(1, &vertexArray)
        glBindVertexArrayOES(vertexArray)
        
        //Generate and bind buffers to our variables then populate them with our model data
        glGenBuffers(1, &indexBuffer)
        glBindBuffer(GLenum(GL_ELEMENT_ARRAY_BUFFER), indexBuffer)
        glBufferData(GLenum(GL_ELEMENT_ARRAY_BUFFER), indices.count*MemoryLayout<GLushort>.stride, indices, GLenum(GL_STATIC_DRAW))
        
        glGenBuffers(1, &vertexBuffer)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), vertexBuffer)
        glBufferData(GLenum(GL_ARRAY_BUFFER), vertices.count*MemoryLayout<GLfloat>.stride, vertices, GLenum(GL_STATIC_DRAW))
        
        glEnableVertexAttribArray(GLuint(effect.vertCoord));
        glVertexAttribPointer(GLuint(effect.vertCoord), 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, nil);
        
        glGenBuffers(1, &textureBuffer)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), textureBuffer)
        glBufferData(GLenum(GL_ARRAY_BUFFER), texCoords.count*MemoryLayout<GLfloat>.stride, texCoords, GLenum(GL_STATIC_DRAW))
        
        glEnableVertexAttribArray(GLuint(effect.texCoord));
        glVertexAttribPointer(GLuint(effect.texCoord), 2, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, nil);
        
        glGenBuffers(1, &normalBuffer)
        glBindBuffer(GLenum(GL_ARRAY_BUFFER), normalBuffer)
        glBufferData(GLenum(GL_ARRAY_BUFFER), normals.count*MemoryLayout<GLfloat>.stride, normals, GLenum(GL_STATIC_DRAW))
        
        glEnableVertexAttribArray(GLuint(effect.normal));
        glVertexAttribPointer(GLuint(effect.normal), 3, GLenum(GL_FLOAT), GLboolean(GL_FALSE), 0, nil);
        
        glBindVertexArrayOES(0)
    }
    
    func tearDown () {
        
        glDeleteBuffers(1, &vertexBuffer)
        glDeleteBuffers(1, &indexBuffer)
        glDeleteBuffers(1, &textureBuffer)
        glDeleteBuffers(1, &normalBuffer)
        
        glDeleteVertexArraysOES(1, &vertexArray)
    }
    
    func update (timeSinceLastUpdate:Double) {
        
        mvm = GLKMatrix4Make(right.x,    right.y,    right.z,    0,
                             up.x,       up.y,       up.z,       0,
                             front.x,    front.y,    front.z,    0,
                             position.x, position.y, position.z, 1);
        
        mvm = GLKMatrix4RotateY(mvm, Float(2.0)*Float(rotationDirection)*Float(timeSinceLastUpdate)) //rotate our triangle for lols
        
        //get our updated vectors back
        right = GLKVector3Make(mvm.m.0, mvm.m.1, mvm.m.2);
        up =    GLKVector3Make(mvm.m.4, mvm.m.5, mvm.m.6);
        front = GLKVector3Make(mvm.m.8, mvm.m.9, mvm.m.10);
    }
    
    func draw () {
        
        EAGLContext.setCurrent(EAGLContext.current())
        glBindVertexArrayOES(vertexArray)
        
        effect.modelViewMatrix = mvm;
        effect.prepareToDraw()
        
        glDrawElements(GLenum(GL_TRIANGLES), GLsizei(indices.count), GLenum(GL_UNSIGNED_SHORT), nil);
    }
}
