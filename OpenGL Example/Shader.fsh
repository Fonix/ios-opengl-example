//Fragment Shader
varying highp vec3 vertexNormal;

uniform highp vec3 lightPosition;
uniform highp vec4 lightDiffuseColour;
uniform highp vec4 lightSpecularColour;
uniform highp vec4 lightAmbientColour;

uniform highp vec3 lightHalfVector;

varying lowp vec2 TexCoordOut;
uniform sampler2D Texture;

void main()
{
    highp float diffuseValue0 = max(dot(vertexNormal, normalize(lightPosition)), 0.0);
    highp vec4 diffuse_color0 = lightDiffuseColour;
    highp vec4 specular_color0 = lightSpecularColour * pow(max(dot(vertexNormal, lightHalfVector), 0.0) , 15.0);
    
    gl_FragColor = texture2D(Texture, TexCoordOut) * ((diffuseValue0 * diffuse_color0) + specular_color0 + lightAmbientColour);
}
