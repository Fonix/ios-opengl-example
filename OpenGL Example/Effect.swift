
import GLKit

class Effect {
    
    //probably should have a separate object to handle the camera info, but for simplicity sake just put everything here
    var cameraPosition = GLKVector3Make(0, 0, 5)
    var cameraLookAt = GLKVector3Make(0, 0, 0)
    var cameraUp = GLKVector3Make(0, 1, 0)
    
    var lightPosition = GLKVector3Make(1, 1, 5);
    var lightDiffuseColour = GLKVector4Make(0.7, 0.7, 0.7, 1);
    var lightSpecularColour = GLKVector4Make(2, 2, 2, 1);
    var lightAmbientColour = GLKVector4Make(0.9, 0.9, 0.9, 1);
    
    var cameraMatrix:GLKMatrix4!
    var modelViewMatrix:GLKMatrix4!
    var camModelViewMatrix:GLKMatrix4!
    var projectionMatrix:GLKMatrix4!
    var modelViewProjectionMatrix:GLKMatrix4!
    
    var textureAndLightingShader:GLuint = 0
    var texture:GLuint = 0
    
    var UNIFORM_MODELVIEWPROJECTION_MATRIX:GLint = -1
    var UNIFORM_NORMAL_MATRIX:GLint = -1
    var UNIFORM_VEC3_lightPosition:GLint = -1
    var UNIFORM_VEC4_lightDiffuseColour:GLint = -1
    var UNIFORM_VEC4_lightSpecularColour:GLint = -1
    var UNIFORM_VEC4_lightAmbientColour:GLint = -1
    var UNIFORM_VEC3_lightHalfVector:GLint = -1
    var UNIFORM_Texture:GLint = -1

    var texCoord:GLint = -1
    var vertCoord:GLint = -1
    var normal:GLint = -1
    
    init () {
        
        setCameraPosition(cameraPosition)
        setCameraLookAt(cameraLookAt)
        
        loadTexture(file: "texture.jpeg", location: &texture)
        loadShaders()
    }
    
    func tearDown () {
        
        glDeleteTextures(1, &texture);
        glDeleteProgram(textureAndLightingShader);
    }
    
    //for moving the camera around
    func setCameraPosition(_ cp:GLKVector3) {
        cameraPosition = cp
        updateCameraMatrix()
    }
    
    //for changing what the camera is pointing to
    func setCameraLookAt(_ cla:GLKVector3) {
        cameraLookAt = cla
        updateCameraMatrix()
    }
    
    func updateCameraMatrix () {
        cameraMatrix = GLKMatrix4MakeLookAt(cameraPosition.x,   cameraPosition.y,   cameraPosition.z,
                                            cameraLookAt.x,     cameraLookAt.y,     cameraLookAt.z,
                                            cameraUp.x,         cameraUp.y,         cameraUp.z)
    }
    
    func loadTexture (file:String, location:inout GLuint) {
        
        let imageRef = UIImage.init(imageLiteralResourceName: file).cgImage! //load image from file into CGImage format
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let bytedata:UnsafeMutableRawPointer = UnsafeMutableRawPointer.allocate(bytes: imageRef.width*imageRef.height*4, alignedTo: MemoryLayout<GLubyte>.stride)
        guard let context = CGContext.init(data: bytedata, width: imageRef.width, height: imageRef.height, bitsPerComponent: 8, bytesPerRow: imageRef.width*4, space: colorSpace, bitmapInfo: UInt32(bitmapInfo.rawValue)) else {
            // cannot create context - handle error
            print("Texture loading failed")
            return
        }
        
        context.draw(imageRef, in: CGRect.init(x: 0, y: 0, width: imageRef.width, height: imageRef.height)) //write image data into our byte array
        
        glGenTextures(1, &location);
        glBindTexture(GLenum(GL_TEXTURE_2D), location);
        
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_MIN_FILTER), GL_LINEAR);
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_S), GL_CLAMP_TO_EDGE); //IMPORTANT FOR NON POWER OF 2 TEXTURES
        glTexParameteri(GLenum(GL_TEXTURE_2D), GLenum(GL_TEXTURE_WRAP_T), GL_CLAMP_TO_EDGE); //if your textures are a power of 2 in dimensions, you can leave this out and it will yeild slightly better performance
        
        glTexImage2D(GLenum(GL_TEXTURE_2D), 0, GL_RGBA, GLsizei(imageRef.width), GLsizei(imageRef.height), 0, GLenum(GL_RGBA), GLenum(GL_UNSIGNED_BYTE), bytedata)
    }
    
    func loadShaders () {
        
        var vertShader:GLuint = 0, fragShader:GLuint = 0
        
        textureAndLightingShader = glCreateProgram() //create a shader to setup
        
        //compile shaders
        let vertShaderPathname = Bundle.main.path(forResource: "Shader", ofType: "vsh")!
        assert(compileShader(shader: &vertShader, type: GLenum(GL_VERTEX_SHADER), file: vertShaderPathname), "Failed to compile vertex shader")

        let fragShaderPathname = Bundle.main.path(forResource: "Shader", ofType: "fsh")!
        assert(compileShader(shader: &fragShader, type: GLenum(GL_FRAGMENT_SHADER), file: fragShaderPathname), "Failed to compile fragment shader")
        
        //attach shaders to program
        glAttachShader(textureAndLightingShader, vertShader)
        glAttachShader(textureAndLightingShader, fragShader)
        
        //link program
        assert(linkProgram(prog: textureAndLightingShader), "Failed to link program")
        
        //get uniform locations from the shader file
        UNIFORM_MODELVIEWPROJECTION_MATRIX =    glGetUniformLocation(textureAndLightingShader, "modelViewProjectionMatrix")
        UNIFORM_NORMAL_MATRIX =                 glGetUniformLocation(textureAndLightingShader, "normalMatrix");
        UNIFORM_VEC3_lightPosition =            glGetUniformLocation(textureAndLightingShader, "lightPosition");
        UNIFORM_VEC4_lightDiffuseColour =       glGetUniformLocation(textureAndLightingShader, "lightDiffuseColour");
        UNIFORM_VEC4_lightSpecularColour =      glGetUniformLocation(textureAndLightingShader, "lightSpecularColour");
        UNIFORM_VEC4_lightAmbientColour =       glGetUniformLocation(textureAndLightingShader, "lightAmbientColour");
        UNIFORM_VEC3_lightHalfVector =          glGetUniformLocation(textureAndLightingShader, "lightHalfVector");
        UNIFORM_Texture =                       glGetUniformLocation(textureAndLightingShader, "Texture");
        //attribute locations
        texCoord =                              glGetAttribLocation(textureAndLightingShader, "TexCoordIn");
        vertCoord =                             glGetAttribLocation(textureAndLightingShader, "position");
        normal =                                glGetAttribLocation(textureAndLightingShader, "normal");
        
        //release vertex and fragment shaders.
        if (vertShader != 0) {
            glDetachShader(textureAndLightingShader, vertShader)
            glDeleteShader(vertShader)
        }
        if (fragShader != 0) {
            glDetachShader(textureAndLightingShader, fragShader)
            glDeleteShader(fragShader)
        }
    }
    
    func compileShader (shader:inout GLuint , type:GLenum, file:String) -> Bool {
        
        var status:GLint = -1

        do {
            let source = try String.init(contentsOfFile: file)
        
            source.withCString({ (ptr:UnsafePointer<GLchar>) in
                var s:UnsafePointer<GLchar>? = ptr
                shader = glCreateShader(type)
                glShaderSource(shader, 1, &s, nil) //requires a cString pointer as an arg so have to do this convoluted method in swift to get it
                glCompileShader(shader)
                glGetShaderiv(shader, GLenum(GL_COMPILE_STATUS), &status);
            })
        }
        catch {
            return false;
        }
        
        #if DEBUG //if there is a problem, it will log it
            var message = [CChar](repeating: CChar(0), count: 256)
            var length = GLsizei(0)
            glGetShaderInfoLog(shader, 256, &length, &message)
            var s = String.init(utf8String: message)!
            if(s.characters.count > 0){print("Shader compile log: \(s)")}
        #endif
        
        if (status != 1) {
            glDeleteShader(shader);
            return false;
        }
        
        return true;
    }
    
    func linkProgram (prog:GLuint) -> Bool {
        
        var status:GLint = -1
        glLinkProgram(prog)
        
        #if DEBUG //if there is a problem, it will log it
            var message = [CChar](repeating: CChar(0), count: 256)
            var length = GLsizei(0)
            glGetProgramInfoLog(prog, 256, &length, &message)
            var s = String.init(utf8String: message)!
            if(s.characters.count > 0){print("Shader link log: \(s)")}
        #endif
        
        glGetProgramiv(prog, GLenum(GL_LINK_STATUS), &status);
        if (status != 1) {
            return false;
        }
        
        return true;
    }
    
    //prepare the shader before drawing an object, your modelviewMatrix per object being drawn should be calculated before calling this
    func prepareToDraw () {
        
        glUseProgram(textureAndLightingShader)
        
        camModelViewMatrix = GLKMatrix4Multiply(cameraMatrix, modelViewMatrix)
        modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, camModelViewMatrix)
        var normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), nil)
        var lightHalfVec = GLKVector3Normalize(GLKVector3Add(cameraPosition, lightPosition))
        
        //write values to the shaders variables
        getPointer(modelViewProjectionMatrix.m) { glUniformMatrix4fv(self.UNIFORM_MODELVIEWPROJECTION_MATRIX, 1, 0, $0) }
        getPointer(normalMatrix.m)              { glUniformMatrix3fv(self.UNIFORM_NORMAL_MATRIX, 1, 0, $0) }
        getPointer(lightPosition.v)             { glUniform3fv(self.UNIFORM_VEC3_lightPosition, 1, $0) }
        getPointer(lightHalfVec.v)              { glUniform3fv(self.UNIFORM_VEC3_lightHalfVector, 1, $0) }
        getPointer(lightDiffuseColour.v)        { glUniform4fv(self.UNIFORM_VEC4_lightDiffuseColour, 1, $0) }
        getPointer(lightAmbientColour.v)        { glUniform4fv(self.UNIFORM_VEC4_lightAmbientColour, 1, $0) }
        getPointer(lightSpecularColour.v)       { glUniform4fv(self.UNIFORM_VEC4_lightSpecularColour, 1, $0) }
        
        glActiveTexture(GLenum(GL_TEXTURE0));
        glBindTexture(GLenum(GL_TEXTURE_2D), texture);
        glUniform1i(UNIFORM_Texture, 0);
    }
    
    //helper func so that getting the unsafe pointer isnt so verbose and ugly
    func getPointer<T:Any>(_ object:T, function:@escaping (_ ptr:UnsafePointer<GLfloat>)->()) {
        var temp = object
        withUnsafePointer(to: &temp) {
            $0.withMemoryRebound(to: GLfloat.self, capacity: MemoryLayout.size(ofValue: temp)) {
                function($0)
            }
        }
    }
}
