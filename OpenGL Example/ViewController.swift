
import UIKit
import GLKit //import the GLKit framework

class ViewController: UIViewController, GLKViewDelegate {

    @IBOutlet weak var glView: GLKView! //outlet to your GLKView in the storyboard
    var displayLink:CADisplayLink!
    var effect:Effect!
    var triangle:Triangle!
    var triangle2:Triangle!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupGLView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tearDownGLView()
    }
    
    func setupGLView () {
        
        if(EAGLContext.current() == nil){
            EAGLContext.setCurrent(EAGLContext.init(api: .openGLES3))  //the context should be created only once throughout the lifetime of your app
        }
        
        //setup the GLView
        glView.delegate = self
        glView.context = EAGLContext.current()
        glView.enableSetNeedsDisplay = true
        glView.drawableDepthFormat = .format24
        
        //enable OpenGL functions that will be used
        glEnable(GLenum(GL_DEPTH_TEST)); //depth tests make sure things that are rendered behind other objects in 3D space dont draw over other previously drawn objects in the buffer
        //glEnable(GLenum(GL_CULL_FACE)); //uncomment this so we dont draw polygons that are facing away from the camera (which would be the inside of a model that you cant see anyway) but for this demo we need the front and back of the triangle
        
        displayLink = CADisplayLink.init(target: self, selector: #selector(render)) //CADisplayLink sets up a function to be called in time with the screens refresh rate
        displayLink.preferredFramesPerSecond = 30
        displayLink.add(to: RunLoop.current, forMode: .defaultRunLoopMode)
        
        effect = Effect() //object for handling our shader, textures and lighting
        
        let aspectRatio = fabsf(Float(glView.frame.width) / Float(glView.frame.height))
        let projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(45.0), aspectRatio, 1.0, 100.0) //determines field of view, the aspect ratio, and the closest and furthest distance objects are drawn
        
        effect.projectionMatrix = projectionMatrix
        
        triangle = Triangle()
        triangle.setup(effect: effect)
        triangle.position = GLKVector3Make(0, 1, 0)
        
        triangle2 = Triangle()
        triangle2.setup(effect: effect)
        triangle2.position = GLKVector3Make(0, -1, 0)
        triangle2.rotationDirection = -1
    }
    
    func tearDownGLView () {
        
        glFlush();
        
        displayLink.invalidate()
        displayLink.remove(from: RunLoop.current, forMode: .defaultRunLoopMode)
        
        triangle.tearDown()
        triangle2.tearDown()
        effect.tearDown()
        
        EAGLContext.setCurrent(nil)
    }
    
    func render () {
        
        glView.display()
    }
    
    func glkView(_ view: GLKView, drawIn rect: CGRect) {
        
        update()
        
        glClearColor(0.0, 0.0, 0.0, 1.0); //clear to black color
        glClear(GLenum(GL_COLOR_BUFFER_BIT) | GLenum(GL_DEPTH_BUFFER_BIT)); //clear the color and depth buffers
        
        triangle.draw()
        triangle2.draw()
    }
    
    var previousTime = CACurrentMediaTime()
    func update () {
        
        let currentTime = CACurrentMediaTime()
        let elapsed = currentTime - previousTime
        previousTime = currentTime
        
        triangle.update(timeSinceLastUpdate: elapsed)
        triangle2.update(timeSinceLastUpdate: elapsed)
    }
}

