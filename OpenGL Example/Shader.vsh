//Vertex shader
attribute vec4 position;
attribute vec3 normal;

varying vec3 vertexNormal;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

uniform vec3 lightPosition;
uniform vec4 lightDiffuseColour;
uniform vec4 lightSpecularColour;
uniform vec4 lightAmbientColour;

uniform vec3 lightHalfVector;

attribute vec2 TexCoordIn;
varying vec2 TexCoordOut;

void main()
{
    vertexNormal = normalize(normalMatrix * normal);
    
    TexCoordOut = TexCoordIn;
    
    gl_Position = modelViewProjectionMatrix * position;
}
